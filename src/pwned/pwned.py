import argparse
import hashlib

import requests
from loguru import logger

CHECK_URL = "https://api.pwnedpasswords.com/range/"
HEADER = {"User-Agent": "password checker"}


class PasswordPwned:
    """
    Checks if your password has been compromised in a data breach.
    """

    @staticmethod
    def check_password(password: str) -> bool:
        """
        Checks if your password has been compromissed in a date breach.

        Args:
            * password - Password to check

        Returns:
            * True - if your password has been compromissed
        """

        logger.info("Calculating checksum for your password")
        sha1 = hashlib.sha1(password.encode("utf-8"))  # nosec
        hash_string = sha1.hexdigest().upper()
        prefix = hash_string[0:5]

        request = requests.get(CHECK_URL + prefix, headers=HEADER).content.decode("utf-8")  # nosec
        hashes = dict(t.split(":") for t in request.split("\r\n"))
        hashes = dict((prefix + key, value) for (key, value) in hashes.items())

        logger.info("Checking if your password exists in databases")
        for item_hash in hashes:
            if item_hash == hash_string:
                logger.info("Pwned")
                logger.debug(
                    f"{password} has previously appeared in "
                    f"a data breach, used {hashes[hash_string]} times, "
                    "and should never be used"
                )
                return True

        logger.info("No pwnage found")
        logger.debug(f"{password} wasn't found in any of the Pwned Passwords")
        return False


def main():
    """
    Main function
    """
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument(
        "--password", "-p", type=str, required=True, dest="password", help="Password to check"
    )
    known_args, _ = arg_parse.parse_known_args()
    print(PasswordPwned.check_password(known_args.password))
