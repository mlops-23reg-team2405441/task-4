import argparse
import sys
from subprocess import PIPE, Popen  # nosec

from loguru import logger


def exec_shell_command(command: str) -> str:
    """
    Common method to execute shell commands.

    Args:
        * comand: Shell command to execute.
                  It could be list of the commands separated by `;`.

    Returns:
        * Output of executed command.
    """

    logger.info(f"Executing command: `{command}`")
    with Popen(command, shell=True, stdout=PIPE, stderr=PIPE) as proc:  # nosec
        status = proc.wait()
        output = proc.stdout.read().decode(sys.stdout.encoding)  # type: ignore
        errors = proc.stderr.read().decode(sys.stderr.encoding)  # type: ignore

        if status != 0:
            raise ValueError(errors)

        logger.debug(output)
        return output


def main():
    """
    Main function
    """
    arg_parse = argparse.ArgumentParser()
    arg_parse.add_argument(
        "--command", "-c", type=str, required=True, dest="command", help="Command to execute"
    )
    known_args, _ = arg_parse.parse_known_args()
    print(exec_shell_command(known_args.command))
